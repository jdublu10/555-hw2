package edu.upenn.cis.cis455.crawler;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

public class SharedCrawlTaskQueue {
	
	//TODO: THIS IS NOT SHARD-SAFE!! IF YOU'RE GOING TO DISTRIBUTE BOLTS, YOU'VE GOTTA PUT THIS IN THE DB!
	private static SharedCrawlTaskQueue INSTANCE;
	
	private PriorityBlockingQueue<CrawlTask> tq;
	
	public static SharedCrawlTaskQueue getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new SharedCrawlTaskQueue();
		}
		
		return INSTANCE;
	}
	
	private SharedCrawlTaskQueue() {
		tq = new PriorityBlockingQueue();
	}

	public CrawlTask poll() {
		return tq.poll();
	}
	
	public void add(CrawlTask t) {
		tq.add(t);
	}

	public synchronized boolean isEmpty() {
		return tq.isEmpty();
	}

	public synchronized int size() {
		return tq.size();
	}

	@Override
	public synchronized String toString() {
		return "";
	}

	
}
