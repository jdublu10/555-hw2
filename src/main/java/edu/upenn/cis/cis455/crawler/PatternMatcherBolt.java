package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.xpath.XPath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class PatternMatcherBolt implements IRichBolt {
	static Logger logger = LogManager.getLogger(PatternMatcherBolt.class);
	
	final static String NAME = "PATTERN_MATCHER_BOLT";
	String executorId = UUID.randomUUID().toString();
	private OutputCollector collector;
	
	final static Fields SCHEMA = new Fields("");
	
	private StorageInterface db;
	private Map<String,XPathEngine> engines;
	private String[] paths;
	
	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(SCHEMA);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.db = StorageFactory.getInstance();
		this.engines = new HashMap<>();
		
		this.paths = db.getChannelsXPaths();
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		return SCHEMA;
	}
	
	@Override
	public void execute(Tuple input) {
		SharedShutdownData.getInstance().startMatching();
		go(input);
		SharedShutdownData.getInstance().stopMatching();
	}

	private void go(Tuple input) {
		OccurrenceEvent e = (OccurrenceEvent) input.getObjectByField("event");
		String url = e.getUrl();
		boolean documentIsXml = e.isFromXmlDocument();
		
		//Get the XPathEngine for this document, or create one if it doesn't exist.
		XPathEngine engine;
		if(engines.keySet().contains(url)) {
			engine = engines.get(url);
		} else {
			String[] allChannels = db.getChannelsXPaths();
			if(allChannels == null) {
				logger.error("Could not get XPath channels from DB, aborting.");
				return;
			}
			engine = XPathEngineFactory.getXPathEngine();
			if(engine == null) {
				logger.error("Could not build XPath engine.");
			}
			logger.debug("New document for this matcher bolt, creating XPath engine with paths: [" + String.join(",", allChannels) + "]");
			engine.setXPaths(allChannels);
			engine.setCaseSensitive(documentIsXml);
		}

		ArrayList<String> matchedChannels = new ArrayList<String>();

		//Evaluate this event.
		boolean[] matches = engine.evaluateEvent(e);
		for(int i = 0; i < matches.length; ++i) {
			if(matches[i]) {
				matchedChannels.add(paths[i]);
			}
		}
		
		//Add this document to all of the matched channels
		for(String path : matchedChannels) {
			logger.info("Match! " + url.toString() + " matches " + path);
			db.addToChannelByXPath(path,url.toString());
		}
		
		//Put the engine back in the map
		engines.put(url.toString(), engine);
	}

}
