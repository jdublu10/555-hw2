package edu.upenn.cis.cis455.crawler;

import java.time.Instant;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.crawler.exceptions.CrawlException;
import edu.upenn.cis.cis455.crawler.models.Directive;
import edu.upenn.cis.cis455.crawler.utils.CrawlerHttpHandler;
import edu.upenn.cis.cis455.crawler.utils.DocumentMetadata;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DocumentFetcherBolt implements IRichBolt {
	static Logger logger = LogManager.getLogger(DocumentFetcherBolt.class);

	String executorId = UUID.randomUUID().toString();
	public static final String NAME = "DOCUMENT_FETCHER_BOLT";
	private OutputCollector collector;

	final static Fields SCHEMA = new Fields("document","delay","url","metadata");

	private int maxSize;

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(SCHEMA);
	}

	@Override
	public void cleanup() {
	}

	
	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;
		this.maxSize = Integer.parseInt(stormConf.get(CrawlerConfig.MAX_SIZE_KEY));

	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);

	}

	@Override
	public Fields getSchema() {
		return null;
	}
	
	@Override
	public void execute(Tuple input) {
		SharedShutdownData.getInstance().startFetching();
		go(input);
		SharedShutdownData.getInstance().stopFetching();
	}

	private void go(Tuple input) {
		CrawlTask t = (CrawlTask) input.getObjectByField("task");
		//TODO: when you distribute these workers... how do they all access the same DB?
		StorageInterface db = StorageFactory.getInstance();

		//There's a (I hope) benign race condition here: the first n workers that hit this will all try to get the robots at once.
		//But it ought to be the same one, so I don't really care.
		Directive directive = db.getOrFetchRobotsFor(t);

		//Skip it if we're disallowed by robots.txt
		if(directive.disallows(t.getURLInfo())) {
			logger.debug(t.getUrl() + " disallowed by robots.txt, aborting.");
			return;
		}

		logger.info("Crawling " + t.getUrl());
		Instant lastCrawled = db.lastCrawled(t.getUrl());

		//Do a HEAD request to download the metadata. We cannot cache this, since
		//it may have changed!
		DocumentMetadata md = CrawlerHttpHandler.getMetadata(t);

		//Do a bunch of checks on the result of the HEAD request to make sure that
		//everything is in order before we make a GET request.
		if(!md.documentFound()) {
			logger.warn("Aborting: response code from HEAD to " + t.getUrl() + " was not 200.");
			return;
		}

		if(!md.smallEnough(maxSize)) {
			logger.debug("Skipping document at " + t.getUrl() + ", too large.");
			return;
		}

		if(!md.contentTypeAllowed()) {
			logger.debug("Skipping document at " + t.getUrl() + ", not an HTML/XML file.");
			return;
		}

		String rawContent;
		//If the document has been modified since we last crawled it, we retrieve the document.
		if(md.lastModifiedAfter(lastCrawled)) {
			logger.info(t.getUrl() + " downloading");
			try {
				rawContent = CrawlerHttpHandler.retrieveDocument(t);
			} catch (CrawlException e) {
				logger.warn("Crawl failed: " + e.getMessage());
				e.printStackTrace();
				return;
			}
		} else {
			//Document is old, but we're still gonna crawl it anyways!
			logger.info(t.getUrl() + " not updated since last crawl");
			rawContent = db.getDocument(t.getUrl());
		}
		
		if(db.documentSeen(rawContent)) {
			logger.debug("Content at " + t.getUrl() + " already seen.");
			return;
		}

		logger.info("New (for this run) content at " + t.getUrl() + ", adding to DB. ");
		db.addDocument(t.getUrl(), rawContent);
		
		logger.info("Emitting document at " + t.getUrl());
		collector.emit(new Values(rawContent,directive.getDelay(),t.getUrl(),md));
	}

}
