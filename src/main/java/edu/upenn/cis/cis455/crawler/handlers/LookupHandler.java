package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;

public class LookupHandler implements Route {
	
	private StorageInterface db;

	public LookupHandler(StorageInterface database) {
		db = database;
	}

	@Override
	public Object handle(Request req, Response res) throws Exception {
		String url = req.queryParams("url");
		if(url == null) {
			res.status(400);
			return "No URL query param given";
		}
		
		String normUrl = (new URLInfo(url)).toString();
		System.out.println("Looking up URL: " + normUrl);
		String doc = db.getDocument(normUrl);
		if(doc == null) {
			res.status(404);
			return "Could not find doc at " + normUrl;
		}
		return doc;
	}

}
