package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;

public class SubscriptionHandler implements Route {

	private StorageInterface db;

	public SubscriptionHandler(StorageInterface database) {
		db = database;
	}

	@Override
	public Object handle(Request req, Response res) throws Exception {
		String channel = req.queryParams("channel");
		if(channel == null) {
			res.status(400);
			return "No channel given";
		}
		
		String name = req.session().attribute("user");
		if(name == null) {
			res.redirect("/login-form");
			return null;
		}
		
		db.subscribeUser(name, channel);
		
		return "Ok, " + name + ", added your subscription to " + channel;
		
		
	}

}
