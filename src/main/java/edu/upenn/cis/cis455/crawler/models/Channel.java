package edu.upenn.cis.cis455.crawler.models;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import edu.upenn.cis.cis455.webappmodels.User;

@Entity
public class Channel {
	
	public static final String XPATH_FIELD_KEY = "xpath";
	public static final String NAME_FIELD_KEY = "name";
	
	@PrimaryKey(sequence="ID")
	private int id;

	@SecondaryKey(relate = Relationship.ONE_TO_ONE)
	private String name;

	@SecondaryKey(relate = Relationship.MANY_TO_ONE)
	private String xpath;
	
//    @SecondaryKey(relate=Relationship.MANY_TO_MANY, relatedEntity=Content.class, name="id")
	//TODO: This should really work! who knows why it doesn't... I'll have to do the join manually, ew.
	private List<Integer> contentIds;
	
//	@SecondaryKey(relate=Relationship.MANY_TO_ONE, relatedEntity=User.class, name="id")
	private int userCreatedId;
	
	private Instant createdAt;
    
    public Channel(String name, String xpath, int userid) {
    	this.name = name;
    	this.xpath = xpath;
    	this.contentIds = new ArrayList<>();
    	this.userCreatedId = userid;
    	this.createdAt = Instant.now();
    }
    
    private Channel() {}
    
    public int getId() {
    	return id;
    }

	public String getXPath() {
		return xpath;
	}
	
	public String getName() {
		return name;
	}
	
	public int getUserCreatedId() {
		return userCreatedId;
	}
	
	public Instant getCreatedAt() {
		return createdAt;
	}
	
	public List<Integer> getContentIds(){
		return contentIds;
	}

	public void addContent(Content c) {
		int id = c.getId();
		if(!contentIds.contains(id)) {
			contentIds.add(c.getId());
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Channel");
		sb.append("[id=");
		sb.append(id);
		sb.append("]");
		sb.append("[name=");
		sb.append(name);
		sb.append("]");
		sb.append("[");
		sb.append("xpath=");
		sb.append(xpath);
		sb.append("]");
		sb.append("[contentIDs=");
		for(Integer id : contentIds) {
			sb.append(id);
			sb.append(",");
		}
		sb.append("]");

		
		return sb.toString();
	}
	

}
