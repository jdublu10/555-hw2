package edu.upenn.cis.cis455.crawler.exceptions;

public class CrawlException extends Exception{
	
	private Reason r;
	
	public CrawlException(Reason r) {
		this.r = r;
	}
	
	public enum Reason {
		CannotOpenConnection,
		MalformedURL, CannotSendHTTPRequest, CannotGetInputStream, CannotReadInputStream, CannotCloseInputStream, CannotRetrieveDocument, CannotParseDocument
	}
	
	@Override
	public String getMessage() {
		switch(r) {
		case CannotOpenConnection:
			return "Cannot open connection";
		case MalformedURL:
			return "Malformed URL";
		default:
			return null;
		}
		
	}

}
