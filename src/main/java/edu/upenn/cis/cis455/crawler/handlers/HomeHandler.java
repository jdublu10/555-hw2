package edu.upenn.cis.cis455.crawler.handlers;

import java.util.List;

import edu.upenn.cis.cis455.crawler.models.Channel;
import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;

public class HomeHandler implements Route {
	
	private StorageInterface db;

	public HomeHandler(StorageInterface database) {
		db = database;
	}

	@Override
	public Object handle(Request req, Response res) throws Exception {
		System.out.println("Requested!");
		List<String> channelNames = db.getAllChannelNames();
		return buildPage(channelNames);
	}
	
	private String buildPage(List<String> channelNames) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<html>");
		sb.append("<head><title>Home</title></head>");
		
		
		sb.append("<body>");
		sb.append("<ul>");
		for(String name : channelNames) {
			sb.append("<li>");
			sb.append("<a href=\"/show?channel=" + name + "\">" + name + "</a>");
			sb.append("</li>");
			
		}
		sb.append("</ul>");
		sb.append("</body>");
		
		sb.append("</html>");
		
		return sb.toString();
	}

}
