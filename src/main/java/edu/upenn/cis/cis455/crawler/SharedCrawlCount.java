package edu.upenn.cis.cis455.crawler;

public class SharedCrawlCount {
	private static SharedCrawlCount INSTANCE;
	private int count;
	private int maxCount;
	
	private SharedCrawlCount (int max) {
		count = 0;
		maxCount = max;
	}
	
	public static void initialize(int max) {
		INSTANCE = new SharedCrawlCount(max);
	}
	
	public static SharedCrawlCount getInstance() {
		return INSTANCE;
	}
	
	public synchronized void inc() {
		if(!isDone()){
			count += 1;
		}
	}
	
	public synchronized boolean isDone() {
		return count >= maxCount;
	}

}
