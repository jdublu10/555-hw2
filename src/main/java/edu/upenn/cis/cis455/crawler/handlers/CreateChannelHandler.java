package edu.upenn.cis.cis455.crawler.handlers;

import edu.upenn.cis.cis455.storage.StorageInterface;
import spark.Request;
import spark.Response;
import spark.Route;

public class CreateChannelHandler implements Route {
	
	private StorageInterface db;

	public CreateChannelHandler(StorageInterface database) {
		this.db = database;
	}

	@Override
	public Object handle(Request req, Response res) throws Exception {
		String channelName = req.params("channelname");
		String xpath = req.queryParams("xpath");
		
		String userName = req.session().attribute("user");
		
		db.createChannel(channelName, xpath, userName);
		
		return userName + ", you've created channel " + channelName + " with xpath " + xpath; 
	}

}
