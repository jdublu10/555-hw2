package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;

import javax.net.ssl.HttpsURLConnection;

import edu.upenn.cis.cis455.crawler.exceptions.CrawlException;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class CrawlTask implements Comparable {
	
	public static final Instant WHENEVER = Instant.MIN;
	
	public static final Comparator<CrawlTask> COMPARATOR = Comparator.comparingLong(CrawlTask::delayTime);

	private URLInfo urlInfo;
	private Instant delayUntil;

	public CrawlTask(String url) {
		this.urlInfo = new URLInfo(url);
		this.delayUntil = WHENEVER;
	}

	public CrawlTask(String url, Instant delayUntil) {
		this.urlInfo = new URLInfo(url);
		this.delayUntil = delayUntil;
	}

	public CrawlTask(URLInfo url, Instant i) {
		this.urlInfo = url;
		this.delayUntil = i;
	}

	public boolean areWeThereYet() {
		return delayUntil.isBefore(Instant.now());
	}

	public String getUrl() {
		return urlInfo.toString();
	}
	
	public URLInfo getURLInfo() {
		return urlInfo;
	}
	
	public long delayTime() {
		return delayUntil.getEpochSecond();
	}

	@Override
	public int compareTo(Object o) {
		return COMPARATOR.compare(this, (CrawlTask) o);
	}

}
