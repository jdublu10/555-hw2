package edu.upenn.cis.cis455.crawler;

import java.time.temporal.ChronoUnit;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.FSMCompiler;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.PathNode;
import edu.upenn.cis.cis455.xpathengine.XPath;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.tuple.Fields;

public class Crawler implements CrawlMaster {
    final static Logger logger = LogManager.getLogger(Crawler.class);
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.

    static final int NUM_WORKERS = 10;

	private static final int INIT_WORKER_COUNT = 10;

	private static final int MAX_WORKER_COUNT = 100;

	private static final long WORKER_TIMEOUT = 15;

	private static final int TASK_QUEUE_SIZE = 100_000;
    
//    private ThreadPoolExecutor workerPool;
//    private CrawlScheduler crawlScheduler;
    
    private int count;
    private int maxSize;
    private String startUrl;
    
    private LocalCluster cluster;
    
    public Crawler(String startUrl, int maxSize, int maxCount) {
    	logger.debug("Creating crawler");
    	this.startUrl = startUrl;
    	this.maxSize = maxSize;
    	
    	SharedCrawlCount.initialize(maxCount);
    	
//    	XPathEngine e = XPathEngineFactory.getXPathEngine();
//    	e.setXPaths(new String[]{"/a/b/c","u/a/b"});
//
//    	System.out.println(e);
//    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open,"u",startUrl, false, 0));
//    	System.out.println(e);
//
//    	System.out.println(e);
//    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open,"a",startUrl, false, 0));
//    	System.out.println(e);
//    	
//    	StorageFactory.getInstance().createChannel("/li/a[contains(text(),\"Law\")]");

    	initCluster();
		
		
		sleepUntilDone();
		stopCluster();
		
    	StorageFactory.getInstance().close();
    	logger.debug("Crawler exiting.");
    	
    }
    
    private void sleepUntilDone() {
    	while(!isDone()) {
			try {
				//Pick a weird number so we don't hit robots.txt-enforced intervals. Why risk a weird concurrency bug?
				Thread.sleep(1234);
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.debug("Waking up main thread to check status...");
    	}
    	logger.debug("Done crawling. Shutting down cluster.");
    	
    	return;
		
	}

	private void initCluster() {
    	CrawlerConfig cfg = new CrawlerConfig();
    	cfg.put(CrawlerConfig.INIT_TASK_KEY, startUrl);
    	cfg.put(CrawlerConfig.MAX_SIZE_KEY, Integer.toString(maxSize));
    	
    	CrawlTaskSpout taskSpout = new CrawlTaskSpout();
    	LinkExtractorBolt extractorBolt = new LinkExtractorBolt();
    	DocumentFetcherBolt fetcherBolt = new DocumentFetcherBolt();
    	DocumentParserBolt parserBolt = new DocumentParserBolt();
    	PatternMatcherBolt matcherBolt = new PatternMatcherBolt();
    	
    	TopologyBuilder builder = new TopologyBuilder();
    	builder.setSpout(CrawlTaskSpout.NAME, taskSpout, 1);
    	builder.setBolt(DocumentFetcherBolt.NAME,fetcherBolt,1).allGrouping(CrawlTaskSpout.NAME);
    	builder.setBolt(LinkExtractorBolt.NAME, extractorBolt, 1).allGrouping(DocumentFetcherBolt.NAME);
    	builder.setBolt(DocumentParserBolt.NAME, parserBolt, 1).allGrouping(DocumentFetcherBolt.NAME);
    	builder.setBolt(PatternMatcherBolt.NAME, matcherBolt, 1).fieldsGrouping(DocumentParserBolt.NAME, new Fields("url"));
    	
    	Topology topo = builder.createTopology();
    	
    	cluster = new LocalCluster();
    	ObjectMapper mapper = new ObjectMapper();
    	
		try {
			String str = mapper.writeValueAsString(topo);

			System.out.println("The StormLite topology is:\n" + str);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		cluster.submitTopology("CRAWLER_TOPO", cfg, topo);
    	
    }
    
    public void stopCluster() {
		cluster.killTopology("CRAWLER_TOPO");
		cluster.shutdown();
    }


    /**
     * We've indexed another document
     */
    @Override
    public void incCount() {
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
    	return SharedShutdownData.getInstance().readyToShutdown() || SharedCrawlCount.getInstance().isDone();
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     */
    public static void main(String args[]) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        String envPath = args[1];
        Integer sizeMb = Integer.valueOf(args[2]);
        Integer size = 1000000 * sizeMb;
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;

        StorageFactory.initDatabaseInstance(envPath);

        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        Crawler crawler = new Crawler(startUrl, size, count);

//        crawler.start();
//        crawler.awaitShutdown();

//        System.out.println("Done crawling!");
    }

}
