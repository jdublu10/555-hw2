package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.DocumentMetadata;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DocumentParserBolt implements IRichBolt {
	static Logger logger = LogManager.getLogger(DocumentParserBolt.class);
	
	final static String NAME = "DOCUMENT_PARSER_BOLT";
	String executorId = UUID.randomUUID().toString();
	private OutputCollector collector;

	final static Fields SCHEMA = new Fields("event","url");

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(SCHEMA);

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		this.collector = collector;

	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		return SCHEMA;
	}
	
	private Values textNode(Element e, String url,boolean isXml,int depth) {
        OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Text,e.tagName(),e.text(),url,isXml,depth);
		return new Values(oe,url);
	}
	
	private Values openElement(Element e, String url, boolean isXml,int depth) {
        OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Open,e.tagName(),url,isXml,depth);
		return new Values(oe,url);
	}
	
	private Values closeElement(Element e, String url, boolean isXml,int depth) {
        OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Close,e.tagName(),url,isXml,depth);
        return new Values(oe,url);
	}
	
	
	private void traverse(Element e, String url, boolean isXml,int depth){
		boolean isRoot = e.tagName().equals("#root");
		
		int nextDepth = isRoot ? depth : depth + 1;
		
		if(!isRoot) {
			collector.emit(openElement(e,url,isXml,depth));
		}
		
		if(e.hasText() && !e.tagName().equals("#root")) {
			collector.emit(textNode(e,url,isXml,depth));
		}
		
		for(Element child : e.children()) {
			traverse(child,url,isXml,nextDepth);
		}
		
		if(!isRoot) {
			collector.emit(closeElement(e,url,isXml,depth));
		}
		
	}
	
	@Override
	public void execute(Tuple input) {
		SharedShutdownData.getInstance().startParsing();
		String url = input.getStringByField("url");
		String document = input.getStringByField("document");
		DocumentMetadata md = (DocumentMetadata) input.getObjectByField("metadata");
		
		boolean isXml = !md.isHTML();
		
		Document d;
		if(md.isHTML()) {
			d = Jsoup.parse(document, url, Parser.htmlParser());
		} else {
			d = Jsoup.parse(document, url, Parser.xmlParser());
		}
		
		traverse(d,url,isXml,0);
		SharedShutdownData.getInstance().stopParsing();
	}
}
