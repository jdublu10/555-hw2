package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Response;
import spark.Route;

public class LogoutHandler implements Route {

	@Override
	public Object handle(Request req, Response res) throws Exception {
		req.session().invalidate();
		res.redirect("/login-form");
		return null;
	}

}
