package edu.upenn.cis.cis455.crawler.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.time.Instant;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.async.AsyncLoggerDefaultExceptionHandler;
import org.hamcrest.core.IsCollectionContaining;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import edu.upenn.cis.cis455.crawler.CrawlTask;
import edu.upenn.cis.cis455.crawler.exceptions.CrawlException;
import edu.upenn.cis.cis455.crawler.models.Directive;

public class CrawlerHttpHandler {
	final static Logger logger = LogManager.getLogger(CrawlerHttpHandler.class);
	final static String CRAWLER_USER_AGENT = "cis455crawler";
	
	
	/**
	 * 
	 * @param url
	 * @return robots.txt file that would cover the given url, and null if there is none.
	 * @throws CrawlException 
	 */
	public static String fetchRobotsTxt(URLInfo url) throws CrawlException {
		URLInfo robotsUrl = url.correspondingRobots();
		
		logger.debug("Fetching robots from " + robotsUrl.toString());

        HttpURLConnection c;
		try {
			c = makeConnection(robotsUrl.toString());
		} catch (CrawlException e1) {
			logger.debug("Could not create connection to fetch robots: "+ e1.getMessage());
			throw new CrawlException(CrawlException.Reason.CannotOpenConnection);
		}
		
		c.setRequestProperty("User-Agent", CRAWLER_USER_AGENT);
		c.setRequestProperty("Accept", "*");
		c.setInstanceFollowRedirects(true);
		
		try {
			c.connect();
		} catch (IOException e) {
			e.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotOpenConnection);
		}
		
		int responseCode;
		try {
			responseCode = c.getResponseCode();
		} catch (IOException e2) {
			e2.printStackTrace();
			return null;
		}
		
		if(responseCode >= 300) {
			c.disconnect();
			return null;
		}
		
		InputStream is;
		try {
			is = c.getInputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotGetInputStream);
		}	
		
		String data;
		try {
			data = new String(is.readAllBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotReadInputStream);
		}
		

		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotCloseInputStream);
		}
		c.disconnect();
		return data;
		

	}
			
	public static DocumentMetadata getMetadata(CrawlTask t) {
		HttpURLConnection c;
		try {
			c = makeConnection(t.getUrl());
		} catch (CrawlException e1) {
			logger.debug("Could not create connection for crawl task: "+ e1.getMessage());
			return null;
		}
		
		c.setRequestProperty("User-Agent", CRAWLER_USER_AGENT);
		c.setRequestProperty("Accept", "*");
		try {
			c.setRequestMethod("HEAD");
		} catch (ProtocolException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		c.setInstanceFollowRedirects(true);
		
		try {
			c.connect();
		} catch (IOException e) {
			logger.debug("Could not send HTTP Request: "+ e.getMessage());
			e.printStackTrace();
			return null;
		}

		int responseCode;
		try {
			responseCode = c.getResponseCode();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		Instant lastModified = Instant.ofEpochMilli(c.getLastModified());

		String cType = c.getContentType();
//		boolean allowedContentType = Arrays.stream(ALLOWED_MIME).anyMatch(m -> cType.contains(m));
		
		c.disconnect();
		
//		if(!allowedContentType) {
//			logger.debug("Skipping document at " + t.getUrl() + ", content type is: " + cType);
//		}
		
//		if(!smallEnough) {
//			logger.debug("Skipping document at " + t.getUrl() + ", has size " + c.getContentLengthLong() + ". Max: " + maxSize);
//		}
		
//		return modifiedSinceLast && allowedContentType && smallEnough;
		return new DocumentMetadata(cType, c.getContentLengthLong(), responseCode, lastModified);
	}

	public static String retrieveDocument(CrawlTask t) throws CrawlException {
		HttpURLConnection c;
		try {
			c = makeConnection(t.getUrl());
		} catch (CrawlException e1) {
			logger.debug("Could not create connection for crawl task: "+ e1.getMessage());
			throw new CrawlException(CrawlException.Reason.CannotOpenConnection);
		}
		
		c.setRequestProperty("User-Agent", CRAWLER_USER_AGENT);
		c.setRequestProperty("Accept", "*");
		try {
			c.setRequestMethod("GET");
		} catch (ProtocolException e2) {
			e2.printStackTrace();
		}
		c.setInstanceFollowRedirects(true);
		
		try {
			c.connect();
		} catch (IOException e) {
			logger.debug("Could not send HTTP Request: "+ e.getMessage());
			e.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotSendHTTPRequest);
		}
		
		InputStream is;
		try {
			is = c.getInputStream();
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotGetInputStream);
		}
		
		String s;
		try {
			s = new String(is.readAllBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotReadInputStream);
		}
//		Reader r = new BufferedReader(new InputStreamReader(is));
//		StringBuilder sb = new StringBuilder();
//		char ch;
//		try {
//			while((ch = (char) r.read()) != -1) {
//				sb.append(ch);
//			}
//		} catch (IOException e1) {
//			e1.printStackTrace();
//			throw new CrawlException(CrawlException.Reason.CannotReadInputStream);
//		}
		 

		
		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotCloseInputStream);
		}
		c.disconnect();
		return s;

	}
	
	public static HttpURLConnection makeConnection(String url) throws CrawlException { 
		try {
			return (HttpURLConnection) (new URL(url)).openConnection();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new CrawlException(CrawlException.Reason.MalformedURL);
		} catch (IOException e) {
			e.printStackTrace();
			throw new CrawlException(CrawlException.Reason.CannotOpenConnection);
		}
	}

}
