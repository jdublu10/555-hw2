package edu.upenn.cis.cis455.crawler.models;

import java.time.Instant;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import edu.upenn.cis.cis455.crawler.utils.InstantMarshaller;
import edu.upenn.cis.cis455.crawler.utils.MD5Hasher;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

@Entity
public class Content {
	
	public static final String URL_FIELD_KEY = "url";
	@PrimaryKey(sequence="ID")
	private int id;
	@SecondaryKey(relate = Relationship.MANY_TO_ONE)
	private String url;
	private String rawContent;
	private Instant lastCrawled;
	
	public Content(String url, String rawContent) {
		this.url = url;
		this.rawContent = rawContent;
		this.lastCrawled = Instant.now();
	}
	
	public Content(String url, String rawContent, Instant lastCrawled) {
		this.url = url;
		this.rawContent = rawContent;
		this.lastCrawled = lastCrawled;
	}

    private Content() {}

	public int getId() {
		return id;
	}

	public String getContent() {
		return rawContent;
	}
	
	public String computeDigest() {
		return MD5Hasher.getDigest(rawContent);
	}

	public Instant getLastCrawled() {
		return lastCrawled;
	}
	
	public String getUrl() {
		return url;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Content");
		sb.append("[id=" + id + "]");
		sb.append("[url=" + url + "]");
		
		return sb.toString();
	}

}
