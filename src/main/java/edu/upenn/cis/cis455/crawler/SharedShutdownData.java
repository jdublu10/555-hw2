package edu.upenn.cis.cis455.crawler;

public class SharedShutdownData {
	private static SharedShutdownData INSTANCE;
	
	private boolean spouting;
	private boolean fetching;
	private boolean parsing;
	private boolean extracting;
	private boolean matching;
	
	private SharedShutdownData() {
		
	}
	
	public static SharedShutdownData getInstance() {
		if(INSTANCE == null) {
			INSTANCE =  new SharedShutdownData();
		} 
		return INSTANCE;
	}
	
	public synchronized boolean readyToShutdown() {
		Boolean queueEmpty = SharedCrawlTaskQueue.getInstance().isEmpty();
		Boolean notSpouting = !spouting;
		Boolean notFetching = !fetching;
		Boolean notParsing = !parsing;
		Boolean notExtracting = !extracting;
		Boolean notMatching = !matching;
		
//		System.out.println(String.join(",", queueEmpty.toString(),notSpouting.toString(),notFetching.toString(),notParsing.toString(),notExtracting.toString(),notMatching.toString())) ;
//		System.out.println(SharedCrawlTaskQueue.getInstance().toString());
		return queueEmpty && notSpouting && notFetching && notParsing && notExtracting && notMatching;
	}
	
	public void startSpouting() {
		spouting = true;
	}
	
	public void stopSpouting() {
		spouting = false;
	}
	
	public void startFetching() {
		fetching = true;
	}
	
	public void stopFetching() {
		fetching = false;
	}
	
	public void startParsing() {
		parsing = true;
	}
	
	public void stopParsing() {
		parsing = false;
	}
	
	public void startExtracting() {
		extracting = true;
	}
	
	public void stopExtracting() {
		extracting = false;
	}
	
	public void startMatching() {
		matching = true;
	}
	
	public void stopMatching() {
		matching = false;
	}
	

}
