package edu.upenn.cis.cis455.crawler.utils;

import java.util.ArrayList;
import java.util.List;

import edu.upenn.cis.cis455.crawler.models.Directive;

public class RobotsHandler {
	
	private static class RuleGroup {
		private String agent;
		private List<String> disallows;
		private Integer delay;
		
		public RuleGroup() {
			agent = "*";
			disallows = new ArrayList();
			delay = 0;
		}
		
		public void setAgent(String a) {
			agent = a;
		}
		
		public void addDisallow(String d) {
			disallows.add(d);
		}
		
		public void setDelay(Integer d) {
			delay = d;
		}
	}
	
	
	
	public static Directive parseRobotsTxt(String hostName, String robots) {
		String[] lines = robots.split("\\r?\\n");
		
		ArrayList<RuleGroup> ruleGroups = new ArrayList();
		RuleGroup curr = new RuleGroup();
		for(String line : lines) {
			if(line.startsWith("#")) {}
			else if(line.isBlank()) {
				ruleGroups.add(curr);
				curr = new RuleGroup();
			} else {
				String[] parts = line.split(":");
				String key = parts[0].toLowerCase().strip();
				String val = parts[1].strip();
				
				switch(key) {
				case "user-agent": curr.setAgent(val); break;
				case "disallow": curr.addDisallow(val); break;
				case "crawl-delay": curr.setDelay(Integer.parseInt(val)); break;
				}
			}
		}
		
		RuleGroup match = mostSpecificMatch(ruleGroups);
		return new Directive(hostName, match.delay,match.disallows);
		
		
	}

	private static RuleGroup mostSpecificMatch(ArrayList<RuleGroup> ruleGroups) {
		RuleGroup curr = null;
		for(RuleGroup r : ruleGroups) {
			if(r.agent.equals("cis455crawler")) {
				return r;
			} else if(r.agent.equals("*")) {
				curr = r;
			}
		}
		return curr;
	}
	
	
}
