package edu.upenn.cis.cis455.crawler.utils;

import java.time.Instant;
import java.util.Arrays;

public class DocumentMetadata {
	
	private final static String[] ALLOWED_MIME = {"text/html", "application/xml", "text/xml" , "+xml"};
	private final static int[] ALLOWED_RESPONSE_CODES = {100, 101, 102, 103, 200, 201, 202, 203, 204, 205, 206, 207, 
														 208, 226, 300, 301, 302, 303, 304, 307, 308};
	private String contentType;
	private long size;
	private Instant lastModified;
	private int responseCode;

	public boolean contentTypeAllowed() {
		return Arrays.stream(ALLOWED_MIME).anyMatch(m -> contentType.contains(m));
	}
	
	public boolean documentFound() {
		return Arrays.stream(ALLOWED_RESPONSE_CODES).anyMatch(m -> responseCode == m);
	}
	
	public boolean isHTML() {
		return contentType.contains("text/html");
	}
	
	public boolean smallEnough(int maxSize) {
		return size <= maxSize;
	}
	
	public boolean lastModifiedAfter(Instant lastCrawled) {
		return lastModified.isAfter(lastCrawled);
	}
	
	public DocumentMetadata(String contentType, long size, int responseCode, Instant lastModified) {
		this.contentType = contentType;
		this.size = size;
		this.lastModified = lastModified;
		this.responseCode = responseCode;
	}
}
