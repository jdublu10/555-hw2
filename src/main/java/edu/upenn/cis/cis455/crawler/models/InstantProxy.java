package edu.upenn.cis.cis455.crawler.models;

import java.time.Instant;

import com.sleepycat.persist.model.Persistent;
import com.sleepycat.persist.model.PersistentProxy;

@Persistent(proxyFor=Instant.class)
public class InstantProxy implements PersistentProxy<Instant> {
	
	private long epochSeconds;
	private int  nsOfSecond;

	@Override
	public void initializeProxy(Instant i) {
		epochSeconds = i.getEpochSecond();
		nsOfSecond = i.getNano();
	}

	@Override
	public Instant convertProxy() {
		return Instant.ofEpochSecond(epochSeconds, nsOfSecond);
	}

}
