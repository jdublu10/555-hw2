package edu.upenn.cis.cis455.crawler.models;

import com.sleepycat.persist.model.DeleteAction;
import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

@Entity
public class ContentDigest {
	@PrimaryKey
	private String digest;
	
	//TODO: maybe we want to be able to join later...
//	@SecondaryKey(relate=Relationship.ONE_TO_ONE, relatedEntity=Content.class, onRelatedEntityDelete=DeleteAction.CASCADE, name="id")
//	private int contentId;
	
	public ContentDigest(String digest) {
		this.digest = digest;
	}
	
	private ContentDigest() {}

}
