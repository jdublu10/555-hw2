package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.PriorityBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class CrawlTaskSpout implements IRichSpout {
	static Logger logger = LogManager.getLogger(CrawlTaskSpout.class);
    private SharedCrawlTaskQueue taskQueue;
    private SpoutOutputCollector collector;

    String executorId = UUID.randomUUID().toString();
    
    final static Fields SCHEMA = new Fields("task");
    
	public static final String NAME = "CRAWL_TASK_SPOUT";
    
    public CrawlTaskSpout() {
    	logger.debug("Starting Crawler Queue Spout");
    }

	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(SCHEMA);
	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
    	logger.debug("Opening Crawler Queue Spout");
		this.collector = collector;
    	taskQueue = SharedCrawlTaskQueue.getInstance();
    	String initTaskUrl = config.get(CrawlerConfig.INIT_TASK_KEY);
    	CrawlTask initTask = new CrawlTask(initTaskUrl);
    	taskQueue.add(initTask);
	}

	@Override
	public void close() {}

	@Override
	public void nextTuple() {
		SharedShutdownData.getInstance().startSpouting();
		CrawlTask t = taskQueue.poll();
		if(t != null && t.areWeThereYet()) {
			logger.debug(executorId + " emitting " + t.getUrl());
			collector.emit(new Values<Object>(t));
		} else if (t != null && !t.areWeThereYet()) {
			taskQueue.add(t);
		}
		SharedShutdownData.getInstance().stopSpouting();
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

}
