package edu.upenn.cis.cis455.crawler;

import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.exceptions.CrawlException;
import edu.upenn.cis.cis455.crawler.models.Directive;
import edu.upenn.cis.cis455.crawler.utils.CrawlerHttpHandler;
import edu.upenn.cis.cis455.crawler.utils.DocumentMetadata;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class LinkExtractorBolt implements IRichBolt {
	final static Logger logger = LogManager.getLogger(LinkExtractorBolt.class);

	String executorId = UUID.randomUUID().toString();
	final static Fields SCHEMA = new Fields("");
	private OutputCollector collector;
	
	final static String NAME = "LINK_EXTRACTOR_BOLT";


	private CrawlScheduler cs;
	private SharedCrawlTaskQueue tq;

	public LinkExtractorBolt() {}


	@Override
	public String getExecutorId() {
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(SCHEMA);
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		logger.info("Preparing Crawler Worker Bolt");
		this.collector = collector;
		//Note: these are not sharded across bolts, so it's possible that they could be out of sync
		this.cs = new CrawlScheduler();

		//Not shard safe! this should be in the DB.
		this.tq = SharedCrawlTaskQueue.getInstance();
	}

	@Override
	public void setRouter(IStreamRouter router) {
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		return SCHEMA;
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
	}

	@Override
	public void execute(Tuple input) {
		SharedShutdownData.getInstance().startExtracting();
		String document = input.getStringByField("document");
		int delay = input.getIntegerByField("delay");
		String urlStr = input.getStringByField("url");

		//If the document hasn't been seen this run, crawl it and add it to the DB.
		Elements links = Jsoup.parse(document, urlStr).select("[href]");

		for(Element linkElem : links) {
			String absLink = linkElem.absUrl("href");
			URLInfo url = new URLInfo(absLink);

			String hostName = url.getHostName();
			Instant i = cs.nextInstant(hostName, delay);
			CrawlTask next = new CrawlTask(url, i);
//			logger.debug("Will crawl " + next.getUrl() + " at " + i.getLong(ChronoField.INSTANT_SECONDS));

			tq.add(next);
		}

		SharedCrawlCount.getInstance().inc();
		SharedShutdownData.getInstance().stopExtracting();
	}


}
