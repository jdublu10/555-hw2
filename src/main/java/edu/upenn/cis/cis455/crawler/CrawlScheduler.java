package edu.upenn.cis.cis455.crawler;

import java.time.Instant;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CrawlScheduler {
	
	private Map<String,Instant> nextInstantMap;
	
	public CrawlScheduler() {
		nextInstantMap = new HashMap<>();
	}
	
	
	public synchronized Instant nextInstant(String hostName, int wait) {
		Instant i = nextInstantMap.get(hostName);

		if(i == null) {
			i = Instant.now();
		}
		
		if(SonicMode.gottaGoFast()) {
			wait = 0;
		}
		
		Instant next = i.plusSeconds(wait);
		nextInstantMap.put(hostName, next);
		return i;
	}
	
}
