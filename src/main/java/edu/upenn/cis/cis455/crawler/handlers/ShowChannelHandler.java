package edu.upenn.cis.cis455.crawler.handlers;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.List;
import java.util.Locale;

import edu.upenn.cis.cis455.crawler.models.Channel;
import edu.upenn.cis.cis455.crawler.models.Content;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.webappmodels.User;
import spark.Request;
import spark.Response;
import spark.Route;

public class ShowChannelHandler implements Route {

	private StorageInterface db;
	private static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("YYYY-MM-DD").withLocale(Locale.UK).withZone(ZoneId.systemDefault());
	private static DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("hh:mm:ss").withLocale(Locale.UK).withZone(ZoneId.systemDefault());
	
	private static String formatInstant(Instant i) {
		return dateFormatter.format(i) + "T" + timeFormatter.format(i);
	}

	public ShowChannelHandler(StorageInterface database) {
		this.db = database;
	}

	@Override
	public Object handle(Request request, Response response) throws Exception {
		String channelName = request.queryParams("channel");
		
		Channel c = db.getChannelByName(channelName);
		if(c == null) {
			System.err.println("Could not find channel named " + channelName);
			response.status(500);
			return null;
		}
		User u = db.getCreatorOfChannel(c);
		if(u == null) {
			System.err.println("Could not find creator of channel named " + channelName);
			response.status(500);
			return null;
		}
		List<Content> matches = db.getDocumentsInChannel(c);
		if(matches == null) {
			System.err.println("Could not find a matching document for channel named " + channelName);
			response.status(500);
			return null;
		}
		
		return buildPage(c,matches,u);

	}
	
	private String buildPage(Channel c, List<Content> matches, User u) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<html>");
		sb.append("<head>");
		sb.append("<title>" + "Channel: " +  c.getName() + "</title>");
		sb.append("</head>");

		sb.append("<body>");

		sb.append("<div class=\"channelheader\">");
		sb.append("<p>Channel name: " + c.getName() + ", created by: " + u.getName() + "</p>");
		sb.append("</div>");
		
		sb.append("<ul>");
		for(Content content : matches) {
			sb.append("<li>");
			sb.append("<p>Location: " + content.getUrl() + "</p>");
			sb.append("<p>Crawled on: " + formatInstant(content.getLastCrawled()) + "</p>");
			sb.append("<div class=\"document\">" + content.getContent() + "</div>");
			sb.append("</li>");
		}
		sb.append("</ul>");
		
		sb.append("</body>");

		sb.append("</html>");
		
		return sb.toString();
	}

}
