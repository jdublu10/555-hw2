package edu.upenn.cis.cis455.crawler.utils;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class InstantMarshaller {
	
	private static DateTimeFormatter dtf = DateTimeFormatter.RFC_1123_DATE_TIME;

	public static String toString(Instant i) {
		ZonedDateTime zdt = i.atZone(ZoneId.of("GMT"));
		return dtf.format(zdt);
	}
	
	public static Instant ofString(String s) {
		return dtf.parse(s, ZonedDateTime::from).toInstant();
	}
	
}
