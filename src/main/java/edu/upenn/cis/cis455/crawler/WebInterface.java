package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.logging.log4j.*;

import static spark.Spark.*;

import edu.upenn.cis.cis455.crawler.handlers.CreateChannelHandler;
import edu.upenn.cis.cis455.crawler.handlers.HomeHandler;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.LogoutHandler;
import edu.upenn.cis.cis455.crawler.handlers.LookupHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;
import edu.upenn.cis.cis455.crawler.handlers.ShowChannelHandler;
import edu.upenn.cis.cis455.crawler.handlers.SubscriptionHandler;

public class WebInterface {
    public static void main(String args[]) {
        org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        port(45555);
        StorageFactory.initDatabaseInstance(args[0]);
        StorageInterface database = StorageFactory.getInstance();
        
        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }

        before("*", testIfLoggedIn);
        
        get("/", new HomeHandler(database));
        
        post("/login", new LoginHandler(database));
        post("/register", new RegistrationHandler(database));
        
        get("/lookup", new LookupHandler(database));
        get("/logout", new LogoutHandler());
        
        get("/create/:channelname",new CreateChannelHandler(database));
        get("/show",new ShowChannelHandler(database));
        
        get("/count", (req,res) -> {
        	return database.getCorpusSize();
        });
        
        get("/subscribe", new SubscriptionHandler(database));

        awaitInitialization();
    }
}
