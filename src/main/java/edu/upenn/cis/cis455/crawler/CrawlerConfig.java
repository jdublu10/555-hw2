package edu.upenn.cis.cis455.crawler;

import edu.upenn.cis.stormlite.Config;

public class CrawlerConfig extends Config {
	static final String INIT_TASK_KEY = "INIT_TASK";
	static final String MAX_SIZE_KEY = "MAX_SIZE";

	public CrawlerConfig() {
		super();
	}

}
