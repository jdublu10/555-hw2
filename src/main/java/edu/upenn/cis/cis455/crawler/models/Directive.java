package edu.upenn.cis.cis455.crawler.models;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import edu.upenn.cis.cis455.crawler.CrawlTask;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;

@Entity
public final class Directive {
		public static final String HOST_NAME_FIELD_KEY = "hostName";

		@PrimaryKey(sequence="ID")
		private int id;

		@SecondaryKey(relate = Relationship.ONE_TO_ONE)
		private String hostName;
		
		private List<String> disallows;
		private int delay;
		
		public boolean disallows(URLInfo u) {
			String filePath = u.getFilePath();
			return disallows.stream().anyMatch(m -> filePath.startsWith(m));
		}
		
		public int getDelay() {
			return delay;
		}
		
		public Directive(String hostName, Integer delay, List<String> ds) {
			disallows = ds;
			//Trim trailing /s
			for(int i = 0; i < disallows.size(); ++i) {
				String s = disallows.get(i);
				if(s.endsWith("/")) {
					s = s.substring(0, s.length() - 1);
				}
				disallows.set(i, s);
			}
			this.delay = delay;
			this.hostName = hostName;
		}
		
		public static Directive fullPermissions(String hostName) {
			return new Directive(hostName, 0, new ArrayList<>());
		}
		
		
		private Directive() {}

	}
