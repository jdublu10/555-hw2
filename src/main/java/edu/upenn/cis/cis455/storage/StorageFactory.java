package edu.upenn.cis.cis455.storage;

public class StorageFactory {
	private static StorageInterface db;

    public static void initDatabaseInstance(String directory) {
        db = new Storage(directory);
    }
    
    public static StorageInterface getInstance() {
    	return db;
    }
}
