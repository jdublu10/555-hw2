package edu.upenn.cis.cis455.storage;

import java.time.Instant;
import java.util.List;

import javax.xml.xpath.XPath;

import edu.upenn.cis.cis455.crawler.CrawlTask;
import edu.upenn.cis.cis455.crawler.models.Channel;
import edu.upenn.cis.cis455.crawler.models.Content;
import edu.upenn.cis.cis455.crawler.models.Directive;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.webappmodels.User;

public interface StorageInterface {

    /**
     * How many documents so far?
     */
    public int getCorpusSize();

    /**
     * Add a new document, getting its ID
     */
    public int addDocument(String url, String documentContents);

    /**
     * Retrieves a document's contents by URL
     */
    public String getDocument(String url);

    /**
     * Adds a user and returns an ID
     */
    public int addUser(String username, String password);

    /**
     * Tries to log in the user, or else throws a HaltException
     */
    public boolean userCanLogin(String username, String password);
    
    /**
     * Shuts down / flushes / closes the storage system
     */
    public void close();

    /**
     * @param string
     * @return true if the document has already been seen this run.
     */
	public boolean documentSeen(String doc);

	public Directive getOrFetchRobotsFor(CrawlTask t);

	/**
	 * Return the instant that the URL was last crawled. If it has never been crawled, we return the initial unix epoch
	 * @param url
	 */
	public Instant lastCrawled(String url);
	
	public void subscribeUser(String username, String channelName);

	/**
	 *  Return an array of all the current channels
	 * @return
	 */
	public String[] getChannelsXPaths();

	/**
	 *  Add a URL to a channel
	 */
	void addToChannelByXPath(String xpath, String url);

	void createChannel(String name, String xpath, String userName);
	
	/*
	 * DEBUGGING
	 */
	
	public void dumpDocuments();
	public void dumpChannels();

	public List<Content> getDocumentsInChannelByName(String channelName);

	public Channel getChannelByName(String channelName);

	public User getCreatorOfChannel(Channel c);

	public List<Content> getDocumentsInChannel(Channel c);

	public List<String> getAllChannelNames();


	
    
}
