package edu.upenn.cis.cis455.storage;

public class UserStorageException extends Exception {
	
	private Reason r;
	
	public UserStorageException(Reason r) {
		this.r = r;
	}
	
	@Override
	public String getMessage() {
		switch(r) {
		case UserExists:
			return "User already exists";
		case UserNotFound:
			return "User not found";
		case UserPasswordMatchFail:
			return "User password does not match";
		default:
			break;
		
		}
		return null;
		
	}
	
	public enum Reason {UserExists, UserNotFound, UserPasswordMatchFail};

}
