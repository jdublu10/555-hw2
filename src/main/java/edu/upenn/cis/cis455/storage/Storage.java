package edu.upenn.cis.cis455.storage;

import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPath;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.nodes.Document;

import edu.upenn.cis.cis455.crawler.CrawlTask;
import edu.upenn.cis.cis455.crawler.exceptions.CrawlException;
import edu.upenn.cis.cis455.crawler.models.Channel;
import edu.upenn.cis.cis455.crawler.models.Content;
import edu.upenn.cis.cis455.crawler.models.Directive;
import edu.upenn.cis.cis455.crawler.utils.CrawlerHttpHandler;
import edu.upenn.cis.cis455.crawler.utils.InstantMarshaller;
import edu.upenn.cis.cis455.crawler.utils.MD5Hasher;
import edu.upenn.cis.cis455.crawler.utils.RobotsHandler;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.webappmodels.PasswordManager;
import edu.upenn.cis.cis455.webappmodels.User;

public class Storage implements StorageInterface {
	final static Logger logger = LogManager.getLogger(Storage.class);

	private StorageManager sm;
	private StorageViews sv;

	public Storage(String dir) {
		this.sm = new StorageManager(dir);
		this.sv = new StorageViews(sm);
	}

	@Override
	public int getCorpusSize() {
		return sv.getCorpusSize();
	}

	@Override
	public int addDocument(String url, String documentContents) {
		return sv.addContent(new Content(url, documentContents));
	}

	@Override
	public String getDocument(String url) {
		Content c = sv.getContentByUrl(url);
		if(c == null) {
			return null;
		}
		return c.getContent();
	}

	@Override
	public int addUser(String username, String password) {
		try {
			return sv.addUser(new User(username, PasswordManager.makePassword(password)));
		} catch (UserStorageException e) {
			logger.debug(e.getMessage());
			//			e.printStackTrace();
			return -1;
		}
	}
	
	@Override
	public boolean userCanLogin(String username, String password) {
		try {
			return sv.getUserCheckPwd(username, password) != null;
		} catch (UserStorageException e) {
			logger.debug(e.getMessage());
			return false;
		}
	}

	@Override
	public void close() {
		sm.cleanup();
	}

	public boolean documentSeen(String doc) {
		return sv.contentSeen(MD5Hasher.getDigest(doc));
	}

	@Override
	public Directive getOrFetchRobotsFor(CrawlTask t) {
		Directive d;
		String hostName = t.getURLInfo().getHostName();
		d = sv.getDirective(hostName);
		//Robots.txt not found locally!
		if(d == null) {
			logger.debug("No cached robots.txt for host: " + hostName + "... fetching.");
			try {
				String robotsFile = CrawlerHttpHandler.fetchRobotsTxt(t.getURLInfo());
				if(robotsFile == null) {
					logger.debug("No robots.txt for " + t.getUrl() + " found, assuming free reign.");
					d = Directive.fullPermissions(hostName);
				} 
				else {
					//If we got a robots file, parse it and figure out the right directive for this crawler.
					d = RobotsHandler.parseRobotsTxt(hostName, robotsFile);
				}

			} catch (CrawlException e1) {
				logger.warn("Failed to fetch robots.txt for: " + t.getUrl());
				e1.printStackTrace();
				d = Directive.fullPermissions(hostName);
			}
			//Now that we have a directive for this hostname, let's set it in the db.
			sv.putDirective(d);
		} else {
			logger.debug("Found robots.txt locally for host: " + hostName);
		}

		return d;

	}

	@Override
	public Instant lastCrawled(String url) {
		return sv.lastCrawled(url);
	}

	@Override
	public String[] getChannelsXPaths() {
		return sv.allXPaths();
	}

	@Override
	public void addToChannelByXPath(String path, String url) {
		sv.addContentToChannelByXPath(path, url);
	}

	@Override
	public void createChannel(String name, String xpath, String userName) {
		User u = sv.getUser(userName);
		if(u == null) {
			logger.error("Could not find user " + userName + " while attempting to create channel " + name);
			return;
		}
		sv.createChannel(name, xpath, u.getId());
	}

	@Override
	public void dumpDocuments() {
		logger.debug(sv.dumpContentDb());
	}

	@Override
	public void dumpChannels() {
		logger.debug(sv.dumpChannelDb());
	}

	@Override
	public void subscribeUser(String username, String channelName) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Content> getDocumentsInChannelByName(String channelName) {
		Channel c = getChannelByName(channelName);
		return getDocumentsInChannel(c);
	}

	@Override
	public Channel getChannelByName(String channelName) {
		return sv.getChannelByName(channelName);
	}

	@Override
	public User getCreatorOfChannel(Channel c) {
		int id = c.getUserCreatedId();
		return sv.getUserById(id);
	}

	@Override
	public List<Content> getDocumentsInChannel(Channel c) {
		//TODO: DO AN ACTUAL JOIN HERE!!!
		List<Integer> contentIds = c.getContentIds();
		List<Content> contents = new ArrayList<>();
		for(Integer id : contentIds) {
			Content content = sv.getContentById(id);
			contents.add(content);
		}
		return contents;
	}

	@Override
	public List<String> getAllChannelNames() {
		return sv.getAllChannelNames();
	}

}
