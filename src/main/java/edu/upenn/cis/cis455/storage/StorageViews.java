package edu.upenn.cis.cis455.storage;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.ClassCatalog;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.collections.StoredKeySet;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.LockMode;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.SecondaryIndex;

import edu.upenn.cis.cis455.crawler.models.Channel;
import edu.upenn.cis.cis455.crawler.models.Content;
import edu.upenn.cis.cis455.crawler.models.ContentDigest;
import edu.upenn.cis.cis455.crawler.models.Directive;
import edu.upenn.cis.cis455.crawler.utils.MD5Hasher;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.webappmodels.User;
import edu.upenn.cis.cis455.xpathengine.XPath;

public class StorageViews {

    final static Logger logger = LogManager.getLogger(StorageViews.class);
	private StorageManager sm;
	private PrimaryIndex<Integer,User> userById;
	private SecondaryIndex<String,Integer,User> userByName;
	
	private PrimaryIndex<Integer,Content> contentById;
	private SecondaryIndex<String,Integer,Content> contentByUrl;
	
	private PrimaryIndex<Integer,Channel> channelById;
	private SecondaryIndex<String,Integer,Channel> channelByXPath;
	private SecondaryIndex<String,Integer,Channel> channelByName;

	private PrimaryIndex<String,ContentDigest> contentDigestByDigest;
	
	private PrimaryIndex<Integer,Directive> directiveById;
	private SecondaryIndex<String,Integer,Directive> directiveByHostName;
	
	static final Boolean USER_WRITE_ALLOWED = true;
	static final boolean SEEN_CONTENT_WRITE_ALLOWED = true;

	public StorageViews(StorageManager sm) {
		this.sm = sm;
		
		userById = sm.getUserStore().getPrimaryIndex(Integer.class, User.class);
		userByName = sm.getUserStore().getSecondaryIndex(userById, String.class, User.NAME_FIELD_KEY);
		
		contentById = sm.getContentStore().getPrimaryIndex(Integer.class, Content.class);
		contentByUrl = sm.getContentStore().getSecondaryIndex(contentById, String.class, Content.URL_FIELD_KEY);
		
		channelById = sm.getChannelStore().getPrimaryIndex(Integer.class, Channel.class);
		channelByXPath = sm.getChannelStore().getSecondaryIndex(channelById,String.class,Channel.XPATH_FIELD_KEY);
		channelByName = sm.getChannelStore().getSecondaryIndex(channelById, String.class,Channel.NAME_FIELD_KEY);

		contentDigestByDigest = sm.getContentDigestStore().getPrimaryIndex(String.class, ContentDigest.class);
		
		directiveById = sm.getDirectiveStore().getPrimaryIndex(Integer.class, Directive.class);
		directiveByHostName = sm.getDirectiveStore().getSecondaryIndex(directiveById, String.class, Directive.HOST_NAME_FIELD_KEY);
	}

	/**
	 * @param contentHash
	 * @return true if the hashed content has already been seen this run.
	 */
	public synchronized boolean contentSeen(String contentHash) {
		return contentDigestByDigest.get(contentHash) != null;
	}
	
	public synchronized int addContent(Content c) {
		contentById.putNoReturn(c);
		contentDigestByDigest.put(new ContentDigest(c.computeDigest()));
		return c.getId();
	}
	

	/**
	 * 
	 * @param k User key to be added
	 * @param u the user to be added.
	 * @return User's ID
	 */
	public synchronized int addUser(User u) throws UserStorageException {
		boolean absent = userById.putNoOverwrite(u);
		if(!absent) {
			throw new UserStorageException(UserStorageException.Reason.UserExists);
		}
		
		return u.getId();
	}
	
	public synchronized User getUserCheckPwd(String name, String pwdAttempt) throws UserStorageException {
		User u = userByName.get(name);
		if(u == null) {
			throw new UserStorageException(UserStorageException.Reason.UserNotFound);
		}
		if (u.passwordIs(pwdAttempt)) {
			return u;
		} else {
			throw new UserStorageException(UserStorageException.Reason.UserPasswordMatchFail);
		}
	}
	
	

	public Directive getDirective(String hostName) {
		return directiveByHostName.get(hostName);
	}

	public void putDirective(Directive d) {
		directiveById.put(d);
	}
	
	public Content getContentByUrl(String url) {
		return contentByUrl.get(url);
	}

	public int getCorpusSize() {
		return (int) contentById.count();
	}

	public Instant lastCrawled(String url) {
		Content c = getContentByUrl(url);
		//I miss the Maybe monad or a ".?" method...
		if(c == null) {
			return Instant.EPOCH;
		} else {
			return c.getLastCrawled();
		}
	}

	public User getUser(String username) {
		return userByName.get(username);
	}

	public String[] allXPaths() {
		EntityCursor cur = channelById.entities();
		ArrayList<String> paths = new ArrayList<String>();
		Channel c;
		while((c = (Channel) cur.next()) != null) {
			paths.add(c.getXPath());
		}
		cur.close();
		
		String[] arr = new String[paths.size()];
		for(int i = 0; i < paths.size(); ++i) {
			arr[i] = paths.get(i);
		}

		return arr;
	}

	public void addContentToChannelByXPath(String path, String url) {
		Content c = getContentByUrl(url);
		if(c == null) {
			logger.warn("Failed to find content under URL " + url + " while adding it to channel " + path);
			return;
		}
		Channel ch = channelByXPath.get(path);
		if(ch == null) {
			logger.error("Could not find channel with XPath " + path);
		} else
		ch.addContent(c);
		channelById.put(ch);
		
	}

	public void createChannel(String name, String xpath, int userId) {
		String normalizedXPath = (new XPath(xpath)).toString();
		Channel ch = new Channel(name, normalizedXPath, userId);
		channelById.put(ch);
	}

	public String dumpContentDb() {
		StringBuilder sb = new StringBuilder();
		EntityCursor<Content> contentCursor = contentById.entities();
		
		Content c = contentCursor.next();
		sb.append("Number of Content-s: " + contentCursor.count());
		while(c != null) {
			sb.append("Content #" + c.getId() + ", from " + c.getUrl());
			sb.append("\n");
		}
		
		contentCursor.close();
		
		return sb.toString();
	}
	
	public String dumpUserDb() {
		StringBuilder sb = new StringBuilder();
		
		EntityCursor<User> userCursor = userById.entities();
		
		User u = userCursor.next();
		
		while(u != null) {
			sb.append(u.getName() + "#" + u.getId());
			sb.append("\n");
			u = userCursor.next();
		}

		userCursor.close();
		
		return sb.toString();
	}

	public String dumpChannelDb() {
		StringBuilder sb = new StringBuilder();
		
		EntityCursor<Channel> channelCursor = channelById.entities();
		
		Channel c = channelCursor.next();
		
		while(c != null) {
			sb.append("Channel: " + c.getXPath() + " with documents: " + c.getContentIds());
			sb.append("\n");
			c = channelCursor.next();
		}

		channelCursor.close();
		
		return sb.toString();
	}

	public Content getContentById(Integer id) {
		return contentById.get(id);
	}

	public User getUserById(int id) {
		return userById.get(id);
	}

	public Channel getChannelByName(String channelName) {
		return channelByName.get(channelName);
	}

	public List<String> getAllChannelNames() {
		List<String> cNames = new ArrayList<>();
		EntityCursor cur = channelByName.keys();
		String cName;
		while((cName = (String) cur.next()) != null) {
			cNames.add(cName);
		}
		cur.close();
		
		return cNames;
	}

}
