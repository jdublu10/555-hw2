package edu.upenn.cis.cis455.storage;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.StoreConfig;
import com.sleepycat.persist.model.AnnotationModel;
import com.sleepycat.persist.model.EntityModel;

import edu.upenn.cis.cis455.crawler.models.InstantProxy;


//TODO: should this be a singleton?
public class StorageManager {
  final static Logger logger = LogManager.getLogger(Storage.class);
	private Environment env;

//	private static final String CLASS_CATALOG_NAME = "java_class_catalog";
//	private StoredClassCatalog javaCatalog;
	
	private static final String USER_STORE_NAME = "UserStore";
	private EntityStore userStore;

	private static final String CONTENT_STORE_NAME = "ContentStore";
	private EntityStore contentStore;

	private static final String CHANNEL_STORE_NAME = "ChannelStore";
	private EntityStore channelStore;
	
	private static final String CONTENT_DIGEST_STORE_NAME = "ContentDigestStore";
	private EntityStore contentDigestStore;
	
	private static final String DIRECTIVE_STORE_NAME = "DirectiveStore";
	private EntityStore directiveStore;
	
//	private static final String SEEN_CONTENT_DB_NAME = "seen_content";
//	private Database seenContentDb;

    public StorageManager(String dir) {
		logger.debug("Launching env in dir: " + dir);

		EnvironmentConfig envConfig = new EnvironmentConfig();
		//TODO: Add more config things here!
		envConfig.setTransactional(true);
		envConfig.setAllowCreate(true);
		env = new Environment(new File(dir), envConfig);
		logger.debug("Env created");
		
        EntityModel em = new AnnotationModel();
        em.registerClass(InstantProxy.class);

        StoreConfig userStoreConfig = new StoreConfig().setAllowCreate(true).setTransactional(true).setModel(em);

        StoreConfig contentStoreConfig = new StoreConfig().setAllowCreate(true).setTransactional(true).setModel(em);
        StoreConfig channelStoreConfig = new StoreConfig().setAllowCreate(true).setTransactional(true).setModel(em);

        StoreConfig contentDigestStoreConfig = new StoreConfig().setAllowCreate(true).setTemporary(true).setModel(em);
        StoreConfig directiveStoreConfig = new StoreConfig().setAllowCreate(true).setTemporary(true).setModel(em);

        
        userStore = new EntityStore(env, USER_STORE_NAME, userStoreConfig);
        contentStore = new EntityStore(env, CONTENT_STORE_NAME, contentStoreConfig);
        channelStore = new EntityStore(env, CHANNEL_STORE_NAME, channelStoreConfig);
        contentDigestStore = new EntityStore(env, CONTENT_DIGEST_STORE_NAME, contentDigestStoreConfig);
        directiveStore = new EntityStore(env,DIRECTIVE_STORE_NAME,directiveStoreConfig);

	}
    
    public Environment getStorageEnv() {
		return env;
	}
    
    public EntityStore getUserStore() {
    	return userStore;
    }
    
    public EntityStore getContentStore() {
    	return contentStore;
    }
    
    public EntityStore getChannelStore() {
    	return channelStore;
    }
    
    public EntityStore getContentDigestStore() {
    	return contentDigestStore;
    }
    
    public EntityStore getDirectiveStore() {
    	return directiveStore;
    }
    
    public void cleanup() {
    	logger.debug("Cleaning up Storage");
		userStore.close();
		contentStore.close();
		channelStore.close();
		contentDigestStore.close();
		directiveStore.close();
		env.close();
    }
}
