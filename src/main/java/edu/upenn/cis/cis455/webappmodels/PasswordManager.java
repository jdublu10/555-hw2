package edu.upenn.cis.cis455.webappmodels;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

public final class PasswordManager {

    private static MessageDigest digest;

	public static byte[] makePassword(String pwdString) {
    	if(digest == null) {
    		try {
				digest = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
    	}

		return digest.digest(pwdString.getBytes(StandardCharsets.UTF_8));
	}
	
	public static boolean passwordIs(byte[] p, String attempt) {
        if(digest == null) {
    		try {
				digest = MessageDigest.getInstance("SHA-256");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
    	}
		return Arrays.equals(p, digest.digest(attempt.getBytes(StandardCharsets.UTF_8)));
	}
}
