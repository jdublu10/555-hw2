package edu.upenn.cis.cis455.webappmodels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
import com.sleepycat.persist.model.Relationship;
import com.sleepycat.persist.model.SecondaryKey;

import edu.upenn.cis.cis455.crawler.models.Channel;

@Entity
public class User implements Serializable {
	
	public static final String NAME_FIELD_KEY = "name";
	
	@PrimaryKey(sequence="ID")
	private int id;
	@SecondaryKey(relate = Relationship.ONE_TO_ONE)
	private String name;
	private byte[] pwd;
	@SecondaryKey(relate = Relationship.MANY_TO_MANY, relatedEntity = Channel.class)
	private List<Integer> subscribedChannelIds;
	
	public User(String n, byte[] bs) {
		name = n;
		pwd = bs;
		subscribedChannelIds = new ArrayList<>();
	}

	public boolean passwordIs(String pwdAttempt) {
		return PasswordManager.passwordIs(pwd,pwdAttempt);
	}

	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
	
	public void addSubscription(Channel c) {
		System.err.println("ERROR UNIMPLMEENTED");
		while(true) {}
	}
	
	public List<Integer> getSubscribedChannelIds(){
		return subscribedChannelIds;
	}
	
    private User() {}


}
