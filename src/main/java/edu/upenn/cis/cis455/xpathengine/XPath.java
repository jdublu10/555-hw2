package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class XPath {
	
	final static class Query{
		enum Type {Equals, Contains};
		private Type type;
		private String query;
		
		public Query(String query, Type type) {
			this.query = query;
			this.type = type;
		}
		
		public Type getType() {
			return type;
		}
		
		public String getString() {
			return query;
		}

		public boolean matches(String text) {
			boolean b;
			switch(type){
			case Contains:
				b = text.contains(query);
				break;
			case Equals:
				b = text.equals(query);
				break;
			default:
				b = false;
			}
			return b;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("[");
				switch(getType()) {
				case Contains:
					sb.append("contains(text(),\"");
					sb.append(query);
					sb.append("\")");
					break;
				case Equals:
					sb.append("text()=\"");
					sb.append(query);
					sb.append("\"");
					break;
				}
				sb.append("]");
			return sb.toString();
		}
	}
	
	final static class PathComponent{
		private static Pattern pcPattern = Pattern.compile("(\\w+)(\\[.+\\])?");
		private static Pattern textPattern = Pattern.compile("\\[\\s*text\\(\\)=\\s*\"(.*)\"\\s*\\]");
		private static Pattern containsPattern = Pattern.compile("\\[\\s*contains\\s*\\(\\s*text\\(\\)\\s*,\"(.*)\"\\s*\\)\\s*\\]");


		private String nodeName;
		private Query query;
		
		public PathComponent(String nodeName) {
			this.nodeName = nodeName;
		}
		
		public PathComponent(String nodeName, Query.Type type, String query) {
			this.nodeName = nodeName;
			this.query = new Query(query,type);
		}
		
		public static PathComponent fromString(String s) {
			Matcher m = pcPattern.matcher(s);
			m.find();
			String nodeName = m.group(1);
			String query = m.group(2);
			if(query == null) {
				return new PathComponent(nodeName);
			} else {
				Matcher mContains = containsPattern.matcher(query);
				Matcher mText = textPattern.matcher(query);
				if(mContains.find()) {
					String containsStr = mContains.group(1);
					return new PathComponent(nodeName, Query.Type.Contains, containsStr);
				} else if (mText.find()) {
					String textStr = mText.group(1);
					return new PathComponent(nodeName, Query.Type.Equals, textStr);
				}
			}
			return null;
		}
		
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("/");
			sb.append(nodeName);
			if(query != null) {
				sb.append(query.toString());
				
			}
			return sb.toString();
		}

		public Query getQuery() {
			return query;
		}
		
		public String getNodeName() {
			return nodeName;
		}
	}
	
	private List<PathComponent> components;
	
	public XPath(String expression) {
		String[] split = Arrays.stream(expression.split("/")).filter(x -> !x.isEmpty()).toArray(String[]::new);
		components = new ArrayList<>();
		for(String exp : split) {
			components.add(PathComponent.fromString(exp));
		}
	}
	
	public String toString() {
		StringBuilder sb =  new StringBuilder();
		for(PathComponent pc : components) {
			sb.append(pc.toString());
		}
		return sb.toString();
	}

	public PathComponent getComponent(int i) {
		return components.get(i);
	}
	
	public int size() {
		return components.size();
	}

	public boolean isTrivial() {
		return components.isEmpty();
	}

}
