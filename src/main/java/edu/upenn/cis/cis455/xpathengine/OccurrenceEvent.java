package edu.upenn.cis.cis455.xpathengine;

/**
 This class encapsulates the tokens we care about parsing in XML (or HTML)
 */
public class OccurrenceEvent {
	public enum Type {Open, Close, Text};
	
	Type type;
	String tag;
	String text;
	String url;
	boolean isXml;
	int depth;
	
	public OccurrenceEvent(Type t, String tag, String text, String url, boolean isXml, int depth) {
		this.type = t;
		this.text = text;
		this.tag = tag;
		this.url = url;
		this.isXml = isXml;
		this.depth = depth;
	}
	
	public OccurrenceEvent(Type t, String tag, String url, boolean isXml, int depth) {
		this.type = t;
		this.text = text;
		this.tag = tag;
		this.url = url;
		this.isXml = isXml;
		this.depth = depth;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getTag() {
		return tag;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	public void setTag(String tag) {
		this.tag= tag;
	}
	

	public String toString() {
		if (type == Type.Open) 
			return "<" + tag + ">";
		else if (type == Type.Close)
			return "</" + tag + ">";
		else if (type == Type.Text)
			return text + " (" + tag + ")";
		else
			return tag;
	}

	public String getUrl() {
		return url;
	}

	public boolean isFromXmlDocument() {
		return isXml;
	}

	public int getDepth() {
		return depth;
	}

}
