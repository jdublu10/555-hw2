package edu.upenn.cis.cis455.xpathengine;

import edu.upenn.cis.cis455.xpathengine.XPath.Query;

/*
 *  PathNodes are a doubly-linked list, so restoring previous state is easier.
 */
public class PathNode {

	enum PathNodeType {Tag, Query}

	public static final String CL_TEXT_KEY = "__CL_TEXT";

	private int id;
	private String nodeName;
	private int level;
	//Called "filters" in the XFilter paper.
	private Query query;
	private PathNodeType type;
	private PathNode next;
	private PathNode prev;


	public PathNode(int id, String nodeName, Query query, int level) {
		this.id = id;
		this.nodeName = nodeName;
		this.query = query;
		this.type = PathNodeType.Query;
		this.level = level;
	}

	public PathNode(int id, String nodeName, int level) {
		this.id = id;
		this.nodeName = nodeName;
		this.query = null;
		this.type = PathNodeType.Tag;
		this.level = level;
	}

	public String getNodeName() {
		return nodeName;
	}
	
	public String candidateListEntry() {
		if(type == PathNodeType.Tag) {
			return nodeName;
		} else {
			return CL_TEXT_KEY;
		}
	}
	
	public int getLevel() {
		return level;
	}
	
	public PathNode getNext() {
		return next;
	}
	
	public void setNext(PathNode p) {
		this.next = p;
	}
	
	public PathNode getPrev() {
		return prev;
	}

	public void setPrev(PathNode p) {
		this.prev = p;
	}
	
	public PathNodeType getType() {
		return type;
	}
	
	public Query getQuery() {
		return query;
	}

	@Override
	public String toString() {
		if(type == PathNodeType.Tag) {
			return id + ":" + nodeName + "[level=" + level + "]";
		} else {
			return id + ":" + nodeName + "[" + query.toString() + "]";
		}
	}

	public int getId() {
		return id;
	}

}
