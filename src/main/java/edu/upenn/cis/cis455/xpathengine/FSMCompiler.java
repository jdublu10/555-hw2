package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import edu.upenn.cis.cis455.xpathengine.XPath.PathComponent;

public class FSMCompiler {
	
	public static PathNode compile(XPath x,int id){
		List<PathNode> nodes = new ArrayList<>();
		
		//Create a list of the nodes
		for(int i = 0; i < x.size(); ++i) {
			PathComponent pc = x.getComponent(i);
			if(pc.getQuery() == null) {
				PathNode p = new PathNode(id, pc.getNodeName(),i);
				nodes.add(p);
			} else {
				PathNode p_tag = new PathNode(id, pc.getNodeName(),i);
				nodes.add(p_tag);
				PathNode p_query = new PathNode(id, pc.getNodeName(),pc.getQuery(),i);
				nodes.add(p_query);
			}
		}
		
		//Forward pass to make back-links
		PathNode prev = null;
		for(PathNode p : nodes) {
			p.setPrev(prev);
			prev = p;
		}
		
		//Backward pass to make forward-links
		Collections.reverse(nodes);
		PathNode next = null;
		for(PathNode p : nodes) {
			p.setNext(next);
			next = p;
		}
		
		
		if(nodes.isEmpty()) {
			return null;
		} else {
			return nodes.get(nodes.size() - 1);
		}
		
	}

}
