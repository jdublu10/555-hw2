package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.xpathengine.XPath.Query;

class XPathEngineImpl implements XPathEngine {
	static Logger logger = LogManager.getLogger(XPathEngineImpl.class);
	
	//Map from element names to current candidate path nodes
	private Map<String,List<PathNode>> candidateList;
	
	private boolean[] status;
	
	private boolean isCaseSensitive = false;

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Status: ");
		for(int i = 0; i < status.length; ++i) {
			sb.append(status[i]);
			sb.append(",");
		}
		sb.append("\n CandidateList: ");
	    String s = candidateList.keySet().stream()
	    	      .map(key -> key + "=" + candidateList.get(key))
	    	      .collect(Collectors.joining(", ", "{", "}"));
	    sb.append(s);
	    return sb.toString();
	}
	
	void clearStatus() {
		for(int i = 0; i < status.length; ++i) {
			status[i] = false;
		}
	}
	
	@Override
	public void setXPaths(String[] expressions) {
		status = new boolean[expressions.length];
		candidateList = new HashMap();

		List<XPath> paths = new ArrayList<>();
		for(String exp : expressions) {
			paths.add(new XPath(exp));
		}
		
		for(int i = 0; i < paths.size(); ++i) {
			XPath x = paths.get(i);
			if(x.isTrivial()) {
				status[i] = true;
				continue;
			}  else {
				status[i] = false;
			}
			PathNode p = FSMCompiler.compile(x,i);
			String firstElt = x.getComponent(0).getNodeName();
			
			candidateList.computeIfAbsent(firstElt, k -> new ArrayList<>()).add(p);
		}
		
		//Ensure that text candidate list is always present.
		candidateList.computeIfAbsent(PathNode.CL_TEXT_KEY, k -> new ArrayList<>());
	}
	
	private void evaluateOpen(String tag, int depth) {
		if(candidateList.containsKey(tag)) {
			List<PathNode> actions = candidateList.remove(tag);
			
			for(PathNode p : actions) {
				assert(p.getType() == PathNode.PathNodeType.Tag);
				if(p.getLevel() == depth) {
					//If there's nowhere left to go, we're done!
					if(p.getNext() == null) {
						status[p.getId()] = true;
					} else {
						String nextSlot = p.getNext().candidateListEntry();
						candidateList.computeIfAbsent(nextSlot, k -> new ArrayList<>()).add(p.getNext());
					}
				}
			}
		}
	}
	
	private void evaluateText(String enclosingTag,String text) {
		List<PathNode> actions = candidateList.get(PathNode.CL_TEXT_KEY);
		List<PathNode> newActions = new ArrayList<>();
		
		for(PathNode p : actions) {
			assert(p.getType() == PathNode.PathNodeType.Query);
			boolean tagsMatch = p.getNodeName().equals(enclosingTag);
			boolean queryPasses = p.getQuery().matches(text);
			//Handling if this is a match: it's not just in the __CL_TEXT bit
			if(tagsMatch && queryPasses) {
				//Handling if we're done or there's more left
				if(p.getNext() == null) {
					status[p.getId()] = true;
				} else {
					String nextSlot = p.getNext().candidateListEntry();
					candidateList.computeIfAbsent(nextSlot, k -> new ArrayList<>()).add(p.getNext());
				}
			} else {
				//If it's not a match, it'll go back on the candidate list.
				newActions.add(p);
			}
		}
		candidateList.put(PathNode.CL_TEXT_KEY, newActions);
//		logger.info(toString());
		
	}

	private void evaluateClose(String tag) {
		//List of nodes whose previous is the tag that just closed.
		List<PathNode> rollback = new ArrayList<>();
		
		Set<String> keys = candidateList.keySet();
		for(String key : keys) {
			List<PathNode> nodes = candidateList.get(key);
			for(PathNode n : nodes) {
				//If there's a place to rollback to, and the place you would rollback to is the tag that just closed!
				if(n.getPrev() != null && n.getPrev().candidateListEntry().equals(tag)) {
					rollback.add(n);
				}
			}
		}
		
		
		for(PathNode p : rollback) {
			//Remove the original node and roll back
			candidateList.get(p.candidateListEntry()).remove(p);
			String prevSlot = p.getPrev().candidateListEntry();
			candidateList.computeIfAbsent(prevSlot, k -> new ArrayList<>()).add(p.getPrev());
		}
	}

	@Override
	public boolean[] evaluateEvent(OccurrenceEvent event) {
		int depth = event.depth;
		String tag;
		if(!isCaseSensitive) {
			tag = event.tag.toLowerCase();
		} else {
			tag = event.tag;
		}
		clearStatus();
		switch(event.type) {
		case Close:
			evaluateClose(tag);
			break;
		case Open:
			evaluateOpen(tag,depth);
			break;
		case Text:
			evaluateText(tag,event.getText());
			break;
		}
		return status;
	}

	

	@Override
	public void setCaseSensitive(boolean b) {
		isCaseSensitive = b;
		if(b) {
			logger.debug("Document is XML, setting to case sensitive matching");
		} else {
			logger.debug("Document is HTML, setting to case insensitive matching");
		}
	}

}
