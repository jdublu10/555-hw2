package edu.upenn.cis.cis455;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngine;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;

public class TestXPathEngineImpl {

	@Test
	public void test() {
		XPathEngine e = XPathEngineFactory.getXPathEngine();
    	e.setXPaths(new String[]{"/a/b/c"});

    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open,"a","", false, 0));
    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open,"b","", false, 1));
    	boolean matches = e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Open,"c","", false, 2))[0];
    	assertTrue(matches);
    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close,"a","", false, 0));
    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close,"b","", false, 1));
    	e.evaluateEvent(new OccurrenceEvent(OccurrenceEvent.Type.Close,"c","", false, 2));
	}

}
