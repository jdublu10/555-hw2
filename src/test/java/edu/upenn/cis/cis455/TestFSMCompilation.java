package edu.upenn.cis.cis455;

import static org.junit.Assert.*;

import org.junit.Test;

import edu.upenn.cis.cis455.xpathengine.FSMCompiler;
import edu.upenn.cis.cis455.xpathengine.PathNode;
import edu.upenn.cis.cis455.xpathengine.XPath;

public class TestFSMCompilation {

	@Test
	public void test() {
		XPath x = new XPath("a/b/c[text()=\"asdf\"]");
		PathNode p = FSMCompiler.compile(x, 0);
		assertEquals(p.candidateListEntry(),"a");
		assertEquals(p.getLevel(),0);
		p = p.getNext();
		assertEquals(p.candidateListEntry(),"b");
		assertEquals(p.getLevel(),1);
		p = p.getNext();
		assertEquals(p.candidateListEntry(),"c");
		assertEquals(p.getLevel(),2);
		p = p.getNext();
		assertEquals(p.candidateListEntry(), PathNode.CL_TEXT_KEY);
		assertEquals(p.getLevel(),2);
	}

}
